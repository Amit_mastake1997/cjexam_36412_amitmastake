CREATE TABLE `Singer` (
  `name` varchar(11) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `age` integer(11) NOT NULL,
  `email_id` varchar(256) NOT NULL,
  `contact` integer(10) NOT NULL,
`rating` float(5) NOT NULL,
  PRIMARY KEY (`email_id`)
);

INSERT INTO Singer VALUES('AMIT','MALE','amit12@gmail.com',913053,5);
Query OK, 1 row affected (0.18 sec)

mysql> INSERT INTO Singer VALUES('ROHAN','MALE','rohanit12@gmail.com',912053,5); 
Query OK, 1 row affected (0.10 sec)

INSERT INTO Singer VALUES('RAMESH','MALE','rameshit12@gmail.com',9120453,5);
Query OK, 1 row affected (0.08 sec)

mysql> INSERT INTO Singer VALUES('YOGITA','FEMALE','rshit12@gmail.com',910453,5);
Query OK, 1 row affected (0.14 sec)

mysql> SELECT *FROM Singer;

