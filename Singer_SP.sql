DELIMITER $$
CREATE PROCEDURE sp_insert_singer( IN pname VARCHAR(50), IN pgender VARCHAR(100), IN page INT, IN pemail_id VARCHAR(100), IN pcontact INT, IN prating FLOAT)
	BEGIN
		INSERT INTO
			Singer( name, gender, age, email_id, contact, rating )
		VALUES
			( pname, pgender, page, pemail_id, pcontact, prating);
	END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE sp_update_singer( IN prating FLOAT, IN pname VARCHAR(100) )
	BEGIN
		UPDATE
			Singer
		SET
			rating=prating
		WHERE
			name=pname;
	END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE sp_delete_singer(IN pname VARCHAR(100) )
	BEGIN
		DELETE
		FROM
			Singer
		WHERE
			name=pname;
	END $$
DELIMITER ;

DELIMITER $$

CREATE PROCEDURE sp_select_singer( )
	BEGIN
		SELECT 
			name, gender, age, email_id, contact,rating
		FROM
			Singer;
	END $$
	
DELIMITER ;
