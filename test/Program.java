package test;

import java.util.List;
import java.util.Scanner;

import dao.DaoLayer;
import pojo.Singer;

public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptRecord(Singer singer) {
		if(  singer != null ) {
			System.out.print("Singer  Name	:	");
			sc.nextLine();
			singer.setName(sc.nextLine());
			System.out.print("Singer Gender	:	");
			singer.setGender(sc.nextLine());
			System.out.print("Email id	:	");
			singer.setEmail_id(sc.nextLine());
			System.out.print("Contatc	:	");
			singer.setContact(sc.nextInt());
			System.out.print("Price	:	");
			singer.setRating(sc.nextInt());
			System.out.print("Singer age	:	");
			singer.setAge(sc.nextInt());
		}
	}
	
	private static void acceptRecord(String[] name) {
		if( name != null ) {
			System.out.print("Enter contatc no	:	");
			name[ 0 ] = sc.nextLine();
		}
	}
	private static void printRecord(Singer singer) {
		if( singer != null )
			System.out.println(singer.toString());
	}
	private static void printRecord(List<Singer> singers) {
		if( singers != null ) {
			singers.forEach(System.out::println);
		}
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Add Singer");
		System.out.println("2.Find Book ");
		System.out.println("3.Update Book ");
		System.out.println("4.Remove Singer");
		System.out.println("5.Print Book(s)");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		try( DaoLayer dao = new DaoLayer()){
			int choice, count;
			String[] name = new String[ 1 ];
			while( ( choice = Program.menuList( ) ) != 0 ){
				Singer singer = null;
				switch( choice ) {
				case 1:
					 singer = new Singer();
					Program.acceptRecord(singer);
					dao.insert(singer);
					break;
				case 2:
					Program.acceptRecord( name );
					singer = dao.find( name[ 0 ] );
					Program.printRecord(singer);
					break;
				case 3:
					Program.acceptRecord(name);
					dao.update(name[0], 5700);
					//TODO
					break;
				case 4:
					Program.acceptRecord( name );
					 dao.delete( name[ 0 ] );
					 //TODO
					break;
				case 5:
					List<Singer> singers = dao.getSingers();
					Program.printRecord( singers );
					break;
				}
			}
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
	}
}