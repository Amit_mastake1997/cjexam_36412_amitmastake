

package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pojo.Singer;
import utils.DBUtils;

public class DaoLayer implements Closeable{
	Connection connection = null;
	CallableStatement stmtInsert;
	CallableStatement stmtUpdate;
	CallableStatement stmtDelete;
	CallableStatement stmtSelect;
	CallableStatement stmtFind;

	public DaoLayer() throws Exception{
		 this.connection = DBUtils.getConnection();
		 this.stmtInsert = this.connection.prepareCall("{call sp_insert_singer(?,?,?,?,?)}");
		 this.stmtUpdate = this.connection.prepareCall("{call sp_update_singer(?,?)}");
		 this.stmtDelete = this.connection.prepareCall("{call sp_delete_singer(?)}");
		 this.stmtSelect = this.connection.prepareCall("{call sp_select_singer()}");
		 this.stmtFind = this.connection.prepareCall("{call sp_find_singer(?)}");
	}
	public int insert(Singer singer) throws Exception{
		this.stmtInsert.setString(1, singer.getName());
		this.stmtInsert.setString(2, singer.getGender());
		this.stmtInsert.setInt(3, singer.getAge());
		this.stmtInsert.setString(4, singer.getEmail_id());
		this.stmtInsert.setInt(5, singer.getContact());
		this.stmtInsert.setFloat(6, singer.getRating());
		this.stmtInsert.execute();
		return stmtInsert.getUpdateCount();
	}
	public int update(String name, float rating) throws Exception{
		this.stmtUpdate.setFloat(1, rating);
		this.stmtUpdate.setString(2, name);
		this.stmtUpdate.execute();
		return stmtUpdate.getUpdateCount();
	}
	public int delete(String name) throws Exception{
		this.stmtDelete.setString(1, name);
		this.stmtDelete.execute();
		return stmtDelete.getUpdateCount();
	}
	
	public Singer find( int contatc ) throws Exception{
		this.stmtFind.setInt(1, contatc);
		if( this.stmtFind.execute() )
		{
			try( ResultSet rs = this.stmtFind.getResultSet()){
				if( rs.next())
					return new Singer(rs.getNString(1), rs.getNString(2), rs.getInt(3), rs.getNString(4),rs.getInt(5), rs.getFloat(6));
			}
		}
		return null;
	}
	public List<Singer> getSingers( )throws Exception{
		List<Singer> singers = new ArrayList<Singer>();
		if( this.stmtSelect.execute() ){
			try( ResultSet rs = this.stmtSelect.getResultSet() ){
				while( rs.next()) {
					Singer singer = new Singer(rs.getString("name"),rs.getString("gender"),rs.getInt("age"),rs.getString("email_id"),rs.getInt("contact"),rs.getFloat("rating"));
					singers.add(singer);
				}
			}
		}
		return singers;
	}
	@Override
	public void close() throws IOException {
		try {
			connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause);
		}
	}
}